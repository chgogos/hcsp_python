import sys
import numpy
from os import listdir
from os.path import isfile, join

__author__ = 'chgogos'


class Importer:
    def __init__(self, fn):
        self.fn = fn

    def read_dataset(self):
        file_object = open(self.fn, "r")
        line1 = file_object.readline()
        s = line1.split()
        problem = HCSProblem(int(s[0]), int(s[1]))
        for t in range(problem.T):
            for p in range(problem.P):
                line = file_object.readline()
                problem.etc[t][p] = float(line)
        file_object.close()
        return problem


class HCSProblem:
    def __init__(self, T, P):
        self.T = T
        self.P = P
        self.etc = []
        for i in range(T):
            self.etc.append([])
            for j in range(P):
                self.etc[i].append(0)

    def sort_by_etc(self, p):
        w = []
        for t in range(self.T):
            w.append(self.etc[t][p])
        v = zip(range(0, self.T), w)
        tmp = sorted(v, key=lambda x: x[1])
        return [x[0] for x in tmp]


class Solution:
    def __init__(self, problem):
        self.problem = problem
        self.schedule = [-1] * problem.T
        self.eft = [0] * problem.P

    def makespan(self):
        return max(self.eft)

    def go_schedule(self, t, p):
        self.schedule[t] = p
        self.eft[p] += self.problem.etc[t][p]


class HeuristicSolver:
    def __init__(self, problem):
        self.problem = problem

    def minimum_completion_time(self):
        tasksList = range(0, self.problem.T)
        sol = Solution(self.problem)
        for t in tasksList:
            minimum = sys.maxsize
            pmin = -1
            for p in range(0, self.problem.P):
                eft = sol.eft[p] + self.problem.etc[t][p]
                if eft < minimum:
                    minimum = eft
                    pmin = p
            sol.go_schedule(t, pmin)
        return sol

    def min_min(self):
        tasksList = list(range(0, self.problem.T))
        sol = Solution(self.problem)
        while len(tasksList) != 0:
            minimum = sys.maxsize
            pmin = -1
            for t in tasksList:
                for p in range(0, self.problem.P):
                    eft = sol.eft[p] + self.problem.etc[t][p]
                    if eft < minimum:
                        minimum = eft
                        tmin = t
                        pmin = p
            sol.go_schedule(tmin, pmin)
            tasksList.remove(tmin)
        return sol

    def min_min_fast(self):
        sortedTasksPerProcessor = numpy.zeros(self.problem.P * self.problem.T, dtype=numpy.int32).reshape(self.problem.P, self.problem.T)
        for p in range(0, self.problem.P):
            sortedTasksPerProcessor[p] = self.problem.sort_by_etc(p)
        return self.schedule_fast_based_on_static_order(sortedTasksPerProcessor);

    def sufferage_list(self):
        sortedTasksPerProcessor = numpy.zeros(self.problem.P * self.problem.T, dtype=numpy.int32).reshape(self.problem.P, self.problem.T)
        min1_etc = numpy.zeros(self.problem.T, dtype=numpy.int32)
        min2_etc = numpy.zeros(self.problem.T, dtype=numpy.int32)
        pmin1_etc = numpy.zeros(self.problem.T, dtype=numpy.int32)
        for t in range(0, self.problem.T):
            min1 = sys.maxsize
            min2 = sys.maxsize
            pmin1 = -1
            for p in range(0, self.problem.P):
                if self.problem.etc[t][p] <= min1:
                    min2 = min1
                    min1 = self.problem.etc[t][p]
                    pmin1 = p
                elif self.problem.etc[t][p] < min2:
                    min2 = self.problem.etc[t][p]
            min1_etc[t] = min1
            min2_etc[t] = min2
            pmin1_etc[t] = pmin1
        for p in range(0, self.problem.P):
            w = []
            for t in range(self.problem.T):
                if p == pmin1_etc[t]:
                    w.append(min2_etc[t]-min1_etc[t])
                else:
                    w.append((min1_etc[t]-problem.etc[t][p]) / problem.etc[t][p])
            v = zip(range(0, self.problem.T), w)
            tmp = sorted(v, key=lambda x: x[1], reverse=True)
            sortedTasksPerProcessor[p] = [x[0] for x in tmp]
        return self.schedule_fast_based_on_static_order(sortedTasksPerProcessor);

    def schedule_fast_based_on_static_order(self, sortedTasksPerProcessor):
        sortedTasksIndices = numpy.zeros(self.problem.P, dtype=numpy.int32)
        scheduledTasks = numpy.zeros(self.problem.T, dtype=numpy.int32)
        sol = Solution(self.problem)
        for i in range(0, self.problem.T):
            for p in range(0, self.problem.P):
                k = sortedTasksIndices[p]
                while scheduledTasks[sortedTasksPerProcessor[p][k]]:
                    k += 1
                sortedTasksIndices[p] = k
            min = sys.maxsize
            pmin=-1
            tmin=-1
            for p in range(0, self.problem.P):
                t2 = int(sortedTasksPerProcessor[p][sortedTasksIndices[p]])
                eft = sol.eft[p] + self.problem.etc[t2][p]
                if eft < min:
                    min = eft
                    tmin = t2
                    pmin = p
            scheduledTasks[tmin]=1
            sol.go_schedule(tmin, pmin)
        return sol;

    def max_min(self):
        tasksList = list(range(0, self.problem.T))
        sol = Solution(self.problem)
        while len(tasksList) != 0:
            maximum = 0
            tmax = -1
            pmax = -1
            for t in tasksList:
                minimum = sys.maxsize
                pmin = -1
                for p in range(0, self.problem.P):
                    eft = sol.eft[p] + self.problem.etc[t][p]
                    if eft < minimum:
                        minimum= eft
                        pmin = p
                if minimum > maximum:
                    maximum = minimum
                    tmax = t
                    pmax = pmin
            sol.go_schedule(tmax, pmax)
            tasksList.remove(tmax)
        return sol

    def sufferage_v2(self):
        tasksList = list(range(0, self.problem.T))
        sol = Solution(self.problem)
        while len(tasksList) != 0:
            suffer_max = 0
            tmax = -1
            for t in tasksList:
                min1 = sys.maxsize
                min2 = sys.maxsize
                for p in range(0, self.problem.P):
                    eft = sol.eft[p] + self.problem.etc[t][p]
                    if eft <= min1:
                        min2 = min1
                        min1 = eft
                    elif eft < min2:
                        min2 = eft
                if (min2-min1) > suffer_max:
                    suffer_max = min2-min1
                    tmax = t
            min = sys.maxsize
            pmin = -1
            for p in range(0, self.problem.P):
                eft = sol.eft[p] + self.problem.etc[tmax][p]
                if eft < min:
                    min = eft
                    pmin = p
            sol.go_schedule(tmax, pmin)
            tasksList.remove(tmax)
        return sol






my_path = "datasets//Braun_et_al"

only_files = [f for f in listdir(my_path) if isfile(join(my_path, f))]
for f in only_files:
    importer = Importer(my_path + "//" + f)
    problem = importer.read_dataset()
    heuristic_solver = HeuristicSolver(problem)
    mct_value = heuristic_solver.minimum_completion_time().makespan()
    min_min_value = heuristic_solver.min_min().makespan()
    min_min_fast_value = heuristic_solver.min_min_fast().makespan()
    max_min_value = heuristic_solver.max_min().makespan()
    sufferage_value = heuristic_solver.sufferage_v2().makespan()
    sufferage_list_value = heuristic_solver.sufferage_list().makespan()

    print("{0} MCT={1:.1f}, Min-Min={2:.1f}, Min-Min-Fast={3:.1f}, Max-Min={4:.1f}, sufferage={5:.1f}, sufferage_list={6:.1f}".format(f,
    mct_value, min_min_value, min_min_fast_value, max_min_value, sufferage_value, sufferage_list_value))
