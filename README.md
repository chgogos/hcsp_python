# HCSP with python
    Scheduling independent tasks using various heuristics
    Heuristics: 
        MCT 
        Min-Min
        Min-Min-Fast
        Max-Min
        Sufferage
        Sufferage-List
       
## Version Control
<https://chgogos@bitbucket.org/chgogos/hcsp_python.git>

## Python version 
3.6

## Python libraries
* numpy


